
data15=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data15/data15_13TeV.periodAllYear_3219ipb.physics_Main.MxAOD.p3704.h024.root
data16=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data16/*.root
data17=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data17/*.root
data18=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data18/*.root

# Select the dataset we want
if [ $1 = data15 ]; then
    FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data15/data15_13TeV.periodAllYear_3219ipb.physics_Main.MxAOD.p3704.h024.root
fi
if [ $1 = data16 ]; then
    FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data16/*.root
fi
if [ $1 = data17 ]; then
    FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data17/*.root
fi
if [ $1 = data18 ]; then
    FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data18/*.root
fi

# Run over all Datasets
for i in ${FILES[@]};
do
    shortname="${i##/*/}"
    echo ${shortname}
    python python/preselection.py ${i} ntuples/TOMERGE_${shortname}
done

# Add together
hadd ntuples/${1}.root ntuples/TOMERGE_*
rm ntuples/TOMERGE_*
