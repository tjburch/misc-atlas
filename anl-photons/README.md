### Code to make samples for ANL photon dip studies

#### To run this code:
----------------

Setup the xAOD framework

    setupATLAS
    asetup AnalysisBase,21.2,latest\[,slc6]

Next setup the directory structure. If you've cloned this, you probably just need to perform a 

    mkdir ntuples/

Then just setup the various make options in the Makefile. The typical MxAOD samples are defined in there.

Variables should be self-documenting in general. A couple of exceptions:

weight - this is just the  MC weight

full_eventweight - this should be the weight value you need when filling a histogram:

xsec * BR * filter efficiency * mcWeight / sum of weights

isPassed - 