#!/usr/bin/python
# Tyler James Burch
print("Importing Modules")
import ROOT
ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
import sys
from array import array
import copy
from collections import OrderedDict

if len(sys.argv) < 3:
    print("Usage: python preselection.py inputfile.root outputfile.root [lumi in pb]")
    sys.exit(1)

# Define Functions to be used
#// ---------------------------------------------------

def propagate_cutflow(f_in, f_out):
    # Propagate cutflow to file
    for key in f_in.GetListOfKeys():
        if "noDalitz_weighted" in key.GetName():
            cutflow_name = key.GetName()

            if cutflow_name is None:
                raise RuntimeError("Unable to get cutflow!")

            cutflow = f_in.Get(cutflow_name)
            f_out.cd()
            cutflow.Write()
            return cutflow      

        elif "_data" in f_in:
            return None


# xAOD Functions
# ---------------------------------------------------
def char2bool(char):
    """Convert char to bool."""
    return {"\x00": False, "\x01": True}[char]


def read(obj, cpp_type, name):
    """Document here."""
    raw_value = obj.auxdataConst(cpp_type)(name)
    if cpp_type == "char":
        return char2bool(raw_value)
    if cpp_type == "float":
        return float(raw_value)
    if "int" in cpp_type:
        return int(raw_value)
    if "long" in cpp_type:
        return int(raw_value)
    return raw_value


def get_tree_dictionary():
    # Defines dictionary with all branches
    long_initializer = 50*[0]

    variables = OrderedDict()    
    # Eventwise
    variables["weight/F"] = array("f", [0])
    variables["full_eventweight/F"] = array("f", [0])
    variables["isPassed/O"] = array("b", [0])
    variables["m_yy/F"] = array("f", [0])
    variables["photon_n/I"] = array('i', [0])
    variables["jet_n/I"] = array('i', [0])

    # Photon Variables
    variables["photon_pt[photon_n]/F"] = array("f", long_initializer)
    variables["photon_eta[photon_n]/F"] = array("f", long_initializer)
    variables["photon_phi[photon_n]/F"] = array("f", long_initializer)
    variables["photon_isTight[photon_n]/O"] = array("b", long_initializer)
    variables["photon_isIsoFixedCutLoose[photon_n]/O"] = array("b", long_initializer)
    variables["photon_isIsoFixedCutTight[photon_n]/O"] = array("b", long_initializer)

    # Jet Variables
    variables["jet_pt[jet_n]/F"] = array("f", long_initializer)
    variables["jet_eta[jet_n]/F"] = array("f", long_initializer)
    variables["jet_phi[jet_n]/F"] = array("f", long_initializer)
    variables["jet_m[jet_n]/F"] = array("f", long_initializer)

    return variables


def get_lumi(filename, lumi=None):
    # Get Luminosity
    if lumi:
        return lumi # this is kind of silly
    elif "mc16a" in filename:
        return 36215.0
    elif "mc16d" in filename:
        return 44307.4
    elif "mc16e" in filename:
        return 58450.1
    elif "/data" in filename:
        return 9999999 # Dummy Value    
    else:
        raise IOError("Lumi can't be figured out by filename - provide as third arg")
    
def get_weights(cutflow):
    if cutflow is None:
        # Case for data
        return 1
    else:
        return (cutflow.GetBinContent(1) / cutflow.GetBinContent(2)) * cutflow.GetBinContent(3)


def main():

    # Get ready for xAOD
    print("Initializing xAOD Framework")
    ROOT.xAOD.Init()

    # Define input and output
    print("Loading Files")
    # ---------------------------------------------------
    f_in = ROOT.TFile.Open(sys.argv[1])
    input_tree = ROOT.xAOD.MakeTransientTree(f_in, "CollectionTree")
    import xAODRootAccess.GenerateDVIterators

    f_out = ROOT.TFile(sys.argv[2], "recreate")
    output_tree = ROOT.TTree("mini", "mini")

    isData = False
    if "/data" in sys.argv[1]:
        isData = True

    print("Getting Cutflow")
    cutflow = propagate_cutflow(f_in, f_out)


    print("Setting up Tree")
    variables = get_tree_dictionary()        
    for v in variables:
        # Hack to get around non-event level variables
        if "[" in v :
            shortname = v.split("[")[0]
        else:
            shortname = v.split("/")[0]        
        output_tree.Branch(
            shortname, # name only
            variables[v], # array
            v
        )

    print("Getting Luminosity and weights")
    if len(sys.argv) == 4:
        lumi = get_lumi(filename=sys.argv[1], lumi=sys.argv[3])
    else:
        lumi = get_lumi(filename=sys.argv[1])

    sum_weight = get_weights(cutflow)

    # Load Tree
    print("Loading up xAOD tree to iterate on")
    entries = input_tree.GetEntries()
    print("%s Entries" % str(entries))

    # Begin Event Loop
    print("Beginning Event Loop")
    # ---------------------------------------------------
    for ientry in range(input_tree.GetEntries()):
        if ientry % 100000 == 0:
            print("On entry {0}. {1:.2f} percent done.".format(ientry,float(ientry)/entries))
        input_tree.GetEntry(ientry)

        # Reset event level variables
        for v in ["full_eventweight/F", "weight/F", "isPassed/O",  "m_yy/F",  "photon_n/I", "jet_n/I"]:
            variables[v][0] = -99

        # Set eventwise variables
        ## Weights
        if not isData:
            variables["weight/F"][0] = read(input_tree.HGamEventInfo, "float", "weight")
            variables["full_eventweight/F"][0] = (
                read(input_tree.HGamEventInfo, "float", "crossSectionBRfilterEff")
                * read(input_tree.HGamEventInfo, "float", "weight")
                * float(lumi)
                / float(sum_weight)
            )
        else: 
            variables["weight/F"][0] = 1
            variables["full_eventweight/F"][0] = 1

        ## Passing Criteria
        variables["isPassed/O"][0] = read(input_tree.HGamEventInfo, "char", "isPassed")


        ## Eventwise quantities
        variables["m_yy/F"][0] = read(input_tree.HGamEventInfo, "float", "m_yy") * 0.001
        variables["jet_n/I"][0] = read(input_tree.HGamEventInfo, "int", "N_j")
        variables["photon_n/I"][0] = len(input_tree.HGamPhotons)

        # Set Photon Variables
        for i,p in enumerate(input_tree.HGamPhotons):
          variables["photon_pt[photon_n]/F"][i] = p.pt() * 0.001
          variables["photon_eta[photon_n]/F"][i] = p.eta()
          variables["photon_phi[photon_n]/F"][i] = p.phi()
          variables["photon_isTight[photon_n]/O"][i] = read(input_tree.HGamPhotons[i], "char", "isTight")
          variables["photon_isIsoFixedCutTight[photon_n]/O"][i] = read(input_tree.HGamPhotons[i], "char", "isIsoFixedCutTight")
          variables["photon_isIsoFixedCutLoose[photon_n]/O"][i] = read(input_tree.HGamPhotons[i], "char", "isIsoFixedCutLoose")
    
        # Fill Tree
        output_tree.Fill()

    # End Event Loop, Save file, finish up
    f_out.cd()
    output_tree.Write()
    f_out.Close()
    ROOT.xAOD.ClearTransientTrees()
    f_in.Close()


if __name__ == "__main__":
    main()